#!/usr/bin/python

from bs4 import BeautifulSoup
from urllib2 import urlopen
import re

BASE_URL = "http://www.albertsons.com"
store_fields = {'store_num': 'og:title', 'street': 'og:street-address', 'city': 'og:locality', 'state': 'og:region', 'zip': 'og:postal-code', 'country': 'og:country-name', 'phone': 'og:phone_number'}

def make_soup(url):
    html = urlopen(url).read()
    return BeautifulSoup(html, "lxml")

def get_state_links(section_url):
    soup = make_soup(section_url)
    state = soup.find("section", "storeListWrap")
    state_links = [BASE_URL + dd.a["href"] for dd in state.findAll("p")]
    return state_links

def get_store_urls(state_url):
    urls = []
    soup = make_soup(state_url)
    # <meta property="og:phone_number" content="(480) 491-1026 "/>
    # See if we have a phone number in the Meta tags which means this 
    # is already a store url
    meta = soup.findAll(attrs={"property":"og:phone_number"}) 
    if meta:
      url = soup.findAll(attrs={"property":"og:url"})
      return [url[0]['content'].encode('utf-8')]
    l = soup.find("section", "storeListWrap")
    store_links = [dd.a["href"] for dd in l.findAll("h3")]
    return store_links

def get_store_info(url):
    store_info = dict()
    soup = make_soup(url)
    for desc,field in store_fields.items():
      meta = soup.findAll(attrs={"property":field})
      if meta:
        if desc == 'store_num':
          name = meta[0]['content'].encode('utf-8')
          match = re.match(r'.*- (\d+)',name)
          if match:
            store_info[desc] = match.group(1)
          else:
            store_info[desc] = 'UNK'
        else:
          store_info[desc] = meta[0]['content'].encode('utf-8')
      else:
        store_info[desc] = ''
    return store_info

if __name__ == '__main__':
  state_list_url = BASE_URL + '/pd/stores/'
  state_links = get_state_links(state_list_url)
  store_urls = []
  for state_url in state_links:
    urls = get_store_urls(state_url)
    if urls:
      store_urls.extend(urls)
  f = open('alb_stores.csv', 'w')
  f.write("Store Number, Street, City, State, Zip, Country, Phone\n")
  for url in store_urls:
     store = get_store_info(url)
     f.write('{},"{}",{},{},{},{},{}\n'.format(store['store_num'], store['street'], store['city'], store['state'], store['zip'], store['country'], store['phone']))
  f.close()
